# FrontEnd Tasks Kata
Bienvenido a la kata FrontEnd Tasks.

La kata consta de 3 iteraciones:
* Conocimientos generales de JavaScript.
* Creación de componentes con React.
* Consumición de datos a través de una API.

## Primera iteración: JavaScript
Implementar la clase `TaskTransformer` que permita tomar las `tareas` de un objeto anidado de manera arbitraria y hacer las siguientes transformaciones:
  * Modificar cada valor de tipo `number` a `boolean` dentro del objeto.\
  Donde valor `<= 0`, es `false`; valor `> 0`, es `true`.
  * Agregar `"Task"` a cada valor de tipo `string` dentro del objeto.

Tener en cuenta:
  * No se debe cambiar la estructura del objeto.
  * Escribir los tests necesarios que validen la correcta transformación del objeto.

## Segunda iteración: React
Construir una página que permita mostrar `tareas` en dos listas diferentes según su estado, `uncompleted` y `completed`. Las `tareas` deben tener un título y un botón que permita modificar su estado, `completed` => `uncompleted` y `uncompleted` => `completed`. Además, el botón debe tener estilos que permitan diferenciar cuando el cursor está sobre y presionando el mismo.

Tener en cuenta que los componentes deben ser:
* Enfocados
* Independientes
* Reutilizables
* Chicos

## Tercera iteración: Consumir datos
Obtener las `tareas` de todos los usuarios desde el recurso `/todos` a través de la API de https://jsonplaceholder.typicode.com. Usar el componente previamente creado para mostrar en dos listas, `completed` y `uncompleted`, las `tareas` según su estado. Implementar, además, estados de carga y error.
