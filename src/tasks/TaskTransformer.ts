export default class TaskTransformer {
  transform(value: any): any {
    if (this.isANumber(value)) {
      return this.transformNumber(value);
    }

    if (this.isAString(value)) {
      return this.transformString(value);
    }

    if (this.isAnArray(value)) {
      return this.transformArray(value);
    }

    if (this.isAnObject(value)) {
      return this.transformObject(value);
    }

    return value;
  }

  private isAnObject(value: any) {
    return typeof value == 'object';
  }

  private transformObject(value: any): any {
    return Object.entries(value).reduce(
      (obj, [key, value]) => ({ ...obj, [key]: this.transform(value) }),
      {}
    );
  }

  private isAnArray(value: any) {
    return Array.isArray(value);
  }

  private transformArray(value: any[]): any {
    return value.map(this.transform, this);
  }

  private isAString(value: any) {
    return typeof value == 'string';
  }

  private transformString(value: string) {
    return `${value} Task`;
  }

  private isANumber(value: any) {
    return typeof value == 'number';
  }

  private transformNumber(value: number) {
    return value > 0;
  }
}
