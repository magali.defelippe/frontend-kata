import { FC, useMemo } from 'react';
import { Task as ITask } from '../App';
import TasksList from './TasksList';

export type BoardProps = {
  onToggleCompletion: (id: number) => void;
  tasks: ITask[];
};

const Board: FC<BoardProps> = ({ onToggleCompletion, tasks }) => {
  const completed = useMemo(
    () => tasks.filter((task) => task.completed),
    [tasks]
  );

  const todos = useMemo(() => tasks.filter((task) => !task.completed), [tasks]);

  return (
    <div className="container">
      <TasksList
        onToggleCompletion={onToggleCompletion}
        tasks={todos}
        title={`${todos.length} todo tasks`}
      />
      <TasksList
        onToggleCompletion={onToggleCompletion}
        tasks={completed}
        title={`${completed.length} completed tasks`}
      />
    </div>
  );
};

export default Board;
