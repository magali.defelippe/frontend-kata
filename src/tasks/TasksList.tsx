import { FC } from 'react';
import { Task as ITask } from '../App';
import List from '../components/List';
import ListItemTask from '../components/ListItemTask';

export type TasksListProps = {
  onToggleCompletion: (id: number) => void;
  tasks: ITask[];
  title?: string;
};

const TasksList: FC<TasksListProps> = ({
  onToggleCompletion,
  tasks,
  title,
}) => {
  return (
    <List title={title}>
      {tasks.map((task) => (
        <ListItemTask
          completed={task.completed}
          id={task.id}
          key={task.id}
          onComplete={onToggleCompletion}
          title={task.title}
        />
      ))}
    </List>
  );
};

export default TasksList;
