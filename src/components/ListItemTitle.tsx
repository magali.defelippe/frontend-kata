import { FC } from 'react';

const ListItemTitle: FC = ({ children }) => (
  <div className="h1">{children}</div>
);

export default ListItemTitle;
