import React, { FC } from 'react';

type ListItemButtonProps = {
  onClick?: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
};

const ListItemButton: FC<ListItemButtonProps> = ({ children, onClick }) => (
  <div className="list-item-button">
    <button className="button" onClick={onClick}>
      {children}
    </button>
  </div>
);

export default ListItemButton;
