import { FC } from 'react';

type ListProps = {
  title?: string;
};

const List: FC<ListProps> = ({ title, children }) => (
  <div className="cards">
    <h1>{title}</h1>
    {children}
  </div>
);

export default List;
