import { FC } from 'react';
import ListItem from './ListItem';
import ListItemButton from './ListItemButton';
import ListItemTitle from './ListItemTitle';

type ListItemTaskProps = {
  completed?: boolean;
  id: number;
  onComplete?: (id: number) => void;
  title?: string;
};

const ListItemTask: FC<ListItemTaskProps> = ({
  completed = false,
  id,
  onComplete = () => {},
  title = 'Untitled',
}) => (
  <ListItem style={{ backgroundColor: completed ? '#1a7e33' : '#a71919' }}>
    <ListItemTitle>{title}</ListItemTitle>
    <ListItemButton
      onClick={() => {
        onComplete(id);
      }}
    >
      {completed ? 'TODO' : 'COMPLETE'}
    </ListItemButton>
  </ListItem>
);

export default ListItemTask;
