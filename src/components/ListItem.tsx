import { CSSProperties, FC } from 'react';

type ListItemProps = {
  style?: CSSProperties;
};

const ListItem: FC<ListItemProps> = ({ children, style }) => (
  <div className={`square`} style={style}>
    {children}
  </div>
);

export default ListItem;
