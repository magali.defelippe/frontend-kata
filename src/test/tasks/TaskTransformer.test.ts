import TaskTransformer from '../../tasks/TaskTransformer';

describe('TaskTransformer should', () => {
  const NUMBER_0 = 0;
  const TITLE = 'Title';
  const ARRAY = [TITLE, 1];
  const ARBITRARY_NESTED_OBJECT = {
    a: [
      {
        a: 'delectus aut autem',
        b: -2,
      },
      {
        a: 2,
        b: 'et porro tempora',
      },
      {
        a: [1, 'quis ut nam facilis et officia qui'],
        b: {
          a: 'quo adipisci enim quam ut ab',
          b: 0,
        },
      },
      {
        a: [
          {
            a: 'fugiat veniam minus',
            b: -1,
          },
          {
            a: 4,
            b: 'vero rerum temporibus dolor',
          },
        ],
        b: 'ipsa repellendus fugit nisi',
      },
    ],
    b: {
      a: -1000,
      b: 'illo expedita consequatur quia in',
    },
  };
  const TRANSFORMED_NUMBER = false;
  const TRANSFORMED_TITLE = `${TITLE} Task`;
  const TRANSFORMED_ARRAY = [TRANSFORMED_TITLE, true];
  const TRANSFORMED_OBJECT = {
    a: [
      {
        a: 'delectus aut autem Task',
        b: false,
      },
      {
        a: true,
        b: 'et porro tempora Task',
      },
      {
        a: [true, 'quis ut nam facilis et officia qui Task'],
        b: {
          a: 'quo adipisci enim quam ut ab Task',
          b: false,
        },
      },
      {
        a: [
          {
            a: 'fugiat veniam minus Task',
            b: false,
          },
          {
            a: true,
            b: 'vero rerum temporibus dolor Task',
          },
        ],
        b: 'ipsa repellendus fugit nisi Task',
      },
    ],
    b: {
      a: false,
      b: 'illo expedita consequatur quia in Task',
    },
  };

  let taskTransformer: TaskTransformer;

  beforeAll(() => {
    taskTransformer = new TaskTransformer();
  });

  test('not transform any values', () => {
    const result = taskTransformer.transform(true);
    expect(result).toBe(true);
  });

  test('transform number values', () => {
    const result = taskTransformer.transform(NUMBER_0);
    expect(result).toBe(TRANSFORMED_NUMBER);
  });

  test('transform string values', () => {
    const result = taskTransformer.transform(TITLE);
    expect(result).toBe(TRANSFORMED_TITLE);
  });

  test('transform array values', () => {
    const result = taskTransformer.transform(ARRAY);
    expect(result).toEqual(TRANSFORMED_ARRAY);
  });

  test('transform arbitrary nested object', () => {
    const result = taskTransformer.transform(ARBITRARY_NESTED_OBJECT);
    expect(result).toEqual(TRANSFORMED_OBJECT);
  });
});
