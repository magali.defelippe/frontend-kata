import axios from 'axios';
import { useEffect, useState } from 'react';
import './App.css';
import Board from './tasks/Board';
export interface Task {
  completed: boolean;
  id: number;
  title: string;
  userId: number;
}

export const App = () => {
  const [tasks, setTasks] = useState<Task[]>([]);

  const getTasks = async () => {
    const response = await axios.get(
      'https://jsonplaceholder.typicode.com/todos/'
    );
    setTasks(response.data);
  };

  useEffect(() => {
    getTasks();
  }, []);

  const toggleCompletion = (id: number) => {
    const state = [...tasks];
    const task = state.find((task) => task.id === id);
    if (!task) return;
    task.completed = !task.completed;
    setTasks(state);
  };

  return <Board onToggleCompletion={toggleCompletion} tasks={tasks} />;
};
